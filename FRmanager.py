import os
import cv2
import numpy as np
import face_recognition
import dlib
import sklearn.metrics.pairwise
import scipy
import math
import time
from orderedset import OrderedSet
from frutils import timeit, check_interactions, makeFilenamesUnique
from DBmanager import FaceDatabaseManager, InteractionDatabaseManager, close_connection
from keras.models import load_model

root = os.getcwd()

class face_recognition_manager:
    def __init__(self,test_data=False, base_db='Database/Face.db', test_db='Database/test.db',
                 unk_db='Database/unknown.db', image_save_dir='Data', data_path='Data'):

        self.data_path = os.path.join(root,data_path)
        self.base_db_path = os.path.join(root,base_db)
        self.base_db = FaceDatabaseManager(os.path.join(root,base_db))
        self.test_db = FaceDatabaseManager(os.path.join(root,test_db))
        self.unk_db = FaceDatabaseManager(os.path.join(root,unk_db))
        self.interaction_db = InteractionDatabaseManager(os.path.join(root,'Database/Interaction.db'))
        self.unk_path = os.path.join(root,unk_db)
        self.check_unk = False
        self.SAVE_VIDEO = False
        self.TEST_DATA = test_data
        self.COSINE = False
        self.image_save_dir = os.path.join(root,image_save_dir)
        self.font = cv2.FONT_HERSHEY_DUPLEX

        self.update_database()  # Run database updates

        self.face, self.label, _ = self.base_db.read_faceinfo()

    def update_database(self):
        """
        Makes the name of images in base_db dir unique
        Updates database of the face recognition system if there are images in base_db dir
        """
        # If the directory does not exist, make it because we will be using this dir to update FR System
        if not os.path.exists(self.data_path):
            os.makedirs(self.data_path)

        if os.path.exists(self.data_path):
            # Read all the dirs in this dir, make the name of images inside them unique
            makeFilenamesUnique(dir=self.data_path)

            if os.path.exists(self.base_db_path):
                if self.TEST_DATA:
                    _, _, self.filenames = self.test_db.read_faceinfo()
                else:
                    _, _, self.filenames = self.base_db.read_faceinfo()

                image_info = self.extract_image_info()
                if len(image_info) > 0:
                    self.save_encodings(image_info)
                print("Face database updated")

            else:
                self.filenames = []
                image_info = self.extract_image_info()
                self.save_encodings(image_info)
                print("Face database created")

        # If no directory but database exists
        elif os.path.exists(self.base_db_path):
            print('Using existing face database')

        else:
            os.makedirs(self.data_path)
            print("Please add images in Data directory. Refer README file for details.")

    def find_encoding(self, image,detector):
        """
        Find encodings that properly represent a face.
        Uses ResNet-34 trained in VGG dataset of 3M faces.
        It has 99.38% accuracy in Labeled Faces in Wild Benchmark
        Returns 128D numpy array that captures different features of a face.
        :param image:
        :return:
        """
        face_locations = face_recognition.face_locations(image)
        # Convert dlib rectangle to (x,y,w,h)
        # gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        # faces = detector(gray_image)
        # # Convert detected face in format required for face recognition
        # face_locations = [self._trim_css_to_bounds(self._rect_to_css(face), gray_image.shape)
        #                   for face in faces]
        if len(face_locations) > 0:
            encoding = face_recognition.face_encodings(image, face_locations)[0]
            return encoding

    def save_encodings(self, face_info):
        """
        Save face informations to database.
        Save it in either Base database or Test database.
        :param face_info:
        :return:
        """
        if self.TEST_DATA:
            self.test_db.save_face_info(face_info)
        else:
            self.base_db.save_face_info(face_info)

    def find_brightness(self, image):
        """
        Find brightness of an image using image histogram
        :param image:
        :return:
        """
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        histogram = cv2.calcHist(gray_image, [0], None, [256], [0, 256])
        pixels = sum(histogram)
        brightness = scale = len(histogram)

        for index in range(0, scale):
            ratio = histogram[index] / pixels
            brightness += ratio * (-scale + index)

        return 1 if brightness == 255 else brightness / scale

    def adjust_gamma(self, image, gamma=1.0):
        """
        Build a lookup table mapping the pixel values [0, 255]
        which corresponds to their adjusted gamma values
        Apply gamma correction using the lookup table
        :param image:
        :param gamma:
        :return:
        """
        table = np.array([((i / 255.0) ** gamma) * 255
                          for i in np.arange(0, 256)]).astype("uint8")

        # apply gamma correction using the lookup table
        return cv2.LUT(image, table)

    def adjust_illumination(self, image):
        """
        Automatically adjust gamma value of an image.
        Adjust gamma to make average brightness (X) close to refrence brightness (R)
        gamma = log10(R)/log10(X) where X is average brightness
        :param image:
        :return:
        """
        ref_brightness = 0.55
        brightness = self.find_brightness(image)[0]
        gamma = math.log10(ref_brightness) / math.log10(brightness)
        adjusted = self.adjust_gamma(image, gamma=gamma)
        return adjusted

    def extract_image_info(self):
        """
        Collect training and test images from directory
        and return images and its label
        :return:
        """
        image_info = []
        detector = dlib.get_frontal_face_detector()

        for dirname, dirnames, filenames in os.walk(self.data_path):
            for subdirname in dirnames:
                subject_path = os.path.join(dirname, subdirname)

                for filename in os.listdir(subject_path):
                    # Check if file already exist in database
                    if filename not in self.filenames:
                        try:
                            image = cv2.imread(os.path.join(subject_path, filename))
                            encoding = self.find_encoding(image,detector)
                            if encoding is None:
                                print('Face not found in ', subject_path+filename)
                            else:
                                print("Adding {} to database".format(filename))
                                info = [subdirname, encoding, filename]
                                image_info.append(info)
                        except Exception as error:
                            print("Error {} in image {}".format(error,subject_path+filename))

        return image_info

    def take_pic(self):
        """
        Take pictures from webcam.
        Make a directory from user name and save pictures in the directory
        :return:
        """
        input_count = 0

        # Take user name and create a folder with the name to save images
        while 1:
            user_name = input('Enter your name:')
            user_dir = os.path.join(self.image_save_dir, user_name)

            if (input_count == 0) & os.path.exists(user_dir):
                print('Warning: User directory already exists!!')
                print('Enter other user name otherwise your image will be saved on same folder')
                input_count += 1
            elif (input_count > 0) & os.path.exists(user_dir):
                image_count = len(os.listdir(user_dir))
                print('{} files found in {} directory'.format(image_count, user_dir))
                print('Saving in already existing directory {}'.format(user_name))
                break
            else:
                os.makedirs(user_dir)
                image_count = 1
                break

        # Capture image from webcam
        video_capture = cv2.VideoCapture(0)
        while True:
            ret, frame = video_capture.read()

            cv2.imshow('Image', frame)

            if cv2.waitKey(1) & 0xFF == ord(' '):
                image_name = user_name + str(image_count) + '.jpg'
                image_path = os.path.join(user_dir, image_name)
                cv2.imwrite(image_path, frame)
                image_count += 1
                print('Image saved')

            # Hit 'q' on the keyboard to quit!
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    def find_distance(self, face_encodings, face_to_compare):
        """
        Find distance between a encoding with a list of encodings
        Use either cosine distance or euclidian distance
        :param face_encodings:
        :param face_to_compare:
        :param cosine:
        :return:
        """
        if self.COSINE:
            return [scipy.spatial.distance.cosine(face_encoding, face_to_compare) for face_encoding in face_encodings]
        else:
            return np.linalg.norm(face_encodings - face_to_compare, axis=1)

    def best_face_match(self, face_encodings, face_to_compare, tolerance):
        """
        Return index of face having lowest distance
        Tolerance is different for cosine similarity and euclidian distance
        For euclidian 0.55 give better result
        For cosine 0.05

        :param face_encodings:
        :param face_to_compare:
        :param tolerance:
        :param cosine:
        :return:
        """
        distances = self.find_distance(face_encodings, face_to_compare)
        min_dist = min(distances)
        if min_dist < tolerance:
            return np.where(distances == min_dist)[0][0]

    def check_unknownface(self, face_encoding):
        """

        :param face_encoding:
        :return:
        """

        if os.path.exists(self.unk_path):
            unk_face, unk_label, _ = self.unk_db.read_faceinfo()
            tot_unk = len(unk_label)
            print(tot_unk)
            if tot_unk > 0:
                unk_index = self.best_face_match(unk_face, face_encoding, tolerance=.55)
                if unk_index:
                    name = unk_label[unk_index]
                else:
                    unk_faceinfo = [['Unknown_' + str(tot_unk + 1),
                                     face_encoding, str(tot_unk + 1)]]
                    self.unk_db.save_face_info(unk_faceinfo)
                    name = "Unknown"
        else:
            unk_faceinfo = [['Uknown_1', face_encoding, '1']]
            self.unk_db.save_face_info(unk_faceinfo)
            name = "Unknown"

        return name

    def recognize_face_in_image(self, face, label, image, face_location, tolerance=0.65):
        """
        Recognize face in image and return name of the person
        Feed the function only after face detection detection
        Specially designed face tracking
        :param face:
        :param label:
        :param image:
        :param face_location:
        :param tolerance:
        :return:
        """
        # face_locations = face_recognition.face_locations(image)
        face_encoding = face_recognition.face_encodings(image, face_location)
        index = self.best_face_match(face, face_encoding[0], tolerance)

        return label[index] if index else None

    def _trim_css_to_bounds(self,css, image_shape):
        """
        Make sure a tuple in (top, right, bottom, left) order is within the bounds of the image.
        """
        return max(css[0], 0), min(css[1], image_shape[1]), min(css[2], image_shape[0]), max(css[3], 0)

    def _rect_to_css(self,rect):
        """
        Convert a dlib 'rect' object to a plain tuple in (top, right, bottom, left) order
        """
        return rect.top(), rect.right(), rect.bottom(), rect.left()

    def rect_to_boundingbox(self,rects):
        # take a bounding predicted by dlib and convert it
        # to the format (x, y, w, h)
        rect_cordinates = []
        for rect in rects:
            x = int(rect.left())
            y = int(rect.top())
            h = int(rect.height())
            w = int(rect.width())
            rect_cordinates.append((x, y, w, h))

        return rect_cordinates

    def return_name(self,name,count):
        """

        :param name:
        :param count:
        :return:
        """
        if name[-1] != "Unknown":
            return True
        elif name[-1] == "Unknown" and count > 5:
            return True
        else:
            return
        
    def realtime_recognition(self, camera_index=0, tolerance=0.5,DEBUG=True,CHATBOT_INTEGRATION=False,FLIP=False,ADDITIONAL_FACEINFO=True):
        """
        Perform realtime recognition

        :param camera_index: index of image capturing device
        :param tolerance: (0 - 1) reducing tolerance make face recognition strict (false recognition decreases
                                  but recognition rate decreases
        :param DEBUG: ( True or False)
        :param CHATBOT_INTEGRATION: (True or False)
        :param FLIP: (True or False)
        :return:
        """
        count = 0
        ROS_PUBLISH_COUNT = 0
        all_recognized_names = []
        process_this_frame = True

        # Face detection model
        print("Loading models")
        detector = dlib.get_frontal_face_detector()
        if ADDITIONAL_FACEINFO:
            model_gender = load_model('./Models/Gender_model.h5')
            model_age = load_model('./Models/Age_model.h5')
            model_emotion = load_model('Models/emotion_recognition.h5')

            # Dictionary for emotion recognition model output and emotions
            emotions = {0: 'Angry', 1: 'Fearful', 2: 'Happy', 3: 'Sad', 4: 'Surprised', 5: 'Neutral'}

        video_capture = cv2.VideoCapture(camera_index)
        if self.SAVE_VIDEO:
            # Define the codec and create VideoWriter object
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            out = cv2.VideoWriter('o.avi', fourcc, 20.0, (640, 480))

        while True:
            start_time = time.time()

            ret, frame = video_capture.read()

            if FLIP:
                frame = cv2.flip(frame, -1)

            # Break from loop if device can't read image
            if not ret:
                break

            # frame1 = self.adjust_illumination(frame)

            # Resize frame of video to 1/4 size for faster face recognition processing
            # small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            small_frame = frame
            gray = cv2.cvtColor(small_frame, cv2.COLOR_BGR2GRAY)

            # Only process every other frame of video to save time
            if process_this_frame:
                face_names = []
                # Find all the faces and face encodings in the current frame of video
                faces = detector(gray)
                # Convert dlib rectangle to (x,y,w,h)

                # Convert detected face in format required for face recognition
                face_locations = [self._trim_css_to_bounds(self._rect_to_css(face), gray.shape)
                                  for face in faces]
                if len(face_locations) > 0:
                    if len(face_locations) > 0:
                        # Extract encoding from detected face
                        face_encodings = face_recognition.face_encodings(small_frame, face_locations)
                        for face_encoding in face_encodings:
                            # Find index of matched face
                            index = self.best_face_match(self.face, face_encoding, tolerance)

                            # Find name using label
                            if index:
                                name = self.label[index]
                            else:
                                # Save face of unknown encodings in unknown.db for future recognition
                                if self.check_unk:
                                    name = self.check_unknownface(face_encoding)
                                else:
                                    name = 'Unknown'

                            face_names.append(name)


            # Uncomment process_this_frame to process alternate frame for face recognition
            #process_this_frame = not process_this_frame

            # Display the results
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                # if previously resized
                top *= 1
                right *= 1
                bottom *= 1
                left *= 1

                if ADDITIONAL_FACEINFO:
                    cropped_face = gray[top:bottom , left:right]
                    test_image = cv2.resize(cropped_face, (48, 48))
                    test_image = test_image.reshape([-1, 48, 48, 1])
                    test_image = np.multiply(test_image, 1.0 / 255.0)

                    probab = model_emotion.predict(test_image)[0] * 100


                    # Finding label from probabilities
                    # Class having highest probability considered output label
                    label = np.argmax(probab)
                    predicted_emotion = emotions[label]

                    #Age and gender recognition
                    cropped2 = frame[top:bottom , left:right, :]

                    face2 = cv2.resize(cropped2, (64, 64))
                    face2 = cv2.normalize(face2, None, alpha=0, beta=255,norm_type=cv2.NORM_MINMAX)

                    faces = np.empty((1, 64, 64, 3))
                    faces[0,:,:,:] = face2
                    predicted_age = model_age.predict(faces)
                    predicted_gender = model_gender.predict(faces)

                    if predicted_gender[0][0] < 0.5:
                        gender_str = 'female'
                    else:
                        gender_str = 'male'

                    age = str(int(predicted_age[0][0]))

                    final_str = name + " is " + predicted_emotion + " " + gender_str + " of age " + age
                # Writing on the frame
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 100, 155), 2)
                cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 100, 155), cv2.FILLED)
                cv2.putText(frame, final_str, (left + 6, bottom - 6), self.font, 0.5, (255, 255, 255), 1)

            if self.SAVE_VIDEO:
                out.write(frame)
            if DEBUG:
                cv2.imshow("Images", np.hstack([frame]))

            # Return detected names
            if CHATBOT_INTEGRATION:
                if len(face_names) > 0:
                    if self.return_name(face_names,count):
                        video_capture.release()
                        cv2.destroyAllWindows()
                        print(face_names)
                        return face_names
                    count += 1

            #print(face_names)
            # Hit 'q' on the keyboard to quit!
            time.sleep(.2)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            # Publish face names in ROS after processing frame total_frame_to_consider times
            if CHATBOT_INTEGRATION:
                total_frame_to_consider = 5
                if ROS_PUBLISH_COUNT % total_frame_to_consider == 0:

                    # Get a flat list from list of lists
                    all_names = [name for face_names in all_recognized_names for name in face_names]

                    # Sort all_names by frequency of name
                    all_names = sorted(all_names, key=all_names.count, reverse=True)
                    print("Names sorted by frequency",all_names)

                    # Not considering names occuring less than two time and avoiding "Unknown"
                    all_names = list(filter(lambda x: all_names.count(x) > 1 and x != "Unknown", all_names))

                    # Get unique names from list in sorted form
                    unique_names = list(OrderedSet(all_names))
                    print("Unique names",unique_names)

                    # Reset variables
                    ROS_PUBLISH_COUNT = 0
                    all_recognized_names = []

                ROS_PUBLISH_COUNT += 1
                all_recognized_names.append(face_names)

            if DEBUG:
                time_taken = time.time() - start_time
                #print("Time taken to complete one iteration of above loop:",time_taken)

        video_capture.release()
        if self.SAVE_VIDEO:
            out.release()

        cv2.destroyAllWindows()

    def log_interaction(self, customer_names):
        """
        Return customer name and his interaction times
        :param customer_names:
        :return:
        """
        assert isinstance(customer_names, list)
        interactions = self.interaction_db.check_db(customer_names=customer_names)
        check_interactions(interactions)

    @timeit
    def test(self, tolerance=0.75):
        """
        Check accuracy, false recognition rate and recognition failure rate.
        Log test informations in a text file
        :param tolerance:
        :return:
        """
        correct_count = 0
        false_count = 0
        detected_names = []

        print("Testing")
        # Read base database and test database
        base_encodings, base_names, _ = self.base_db.read_faceinfo()
        test_encodings, test_names, _ = self.test_db.read_faceinfo()

        # Perform comparision of test face encodings with base encodings
        for i, encodings in enumerate(test_encodings):
            match_index = self.best_face_match(base_encodings, encodings, tolerance)
            if match_index:
                if test_names[i] == base_names[match_index]:
                    user_name = base_names[match_index]
                    correct_count += 1
                else:
                    user_name = base_names[match_index]
                    false_count += 1
            else:
                user_name = 'UNK'

            detected_names.append(user_name)

        # Getting counts of images
        recog_fail_count = detected_names.count('UNK')
        train_image_count = base_names.count(test_names[0])
        test_image_count = len(test_names)

        # Finding test results
        accuracy = correct_count / test_image_count * 100
        false_recognition_rate = false_count / test_image_count * 100
        recognition_fail_rate = recog_fail_count / test_image_count * 100

        if not os.path.exists('TestLog.txt'):
            file = open("TestLog.txt", "w+")
            file.close

        # Write test findings to log file
        with open("TestLog.txt", "r+") as test_log_file:
            test_count = len(test_log_file.readlines()) // 8 + 1
            print(test_count)
            test_log_string = "\n".join(["Test {} #Using Euclidian distance",
                                         "Total train images: {}",
                                         "Total test image: {}", "Tolerance: {}",
                                         "Accuracy: {}", "False recognition Rate: {}",
                                         "Recognition failed rate : {} \n\n"])
            test_log = test_log_string.format(test_count, train_image_count,
                                              test_image_count, tolerance,
                                              accuracy, false_recognition_rate,
                                              recognition_fail_rate)

            test_log_file.write(test_log)

        print("Accuracy:", accuracy)
        print('Error rate:', false_recognition_rate)
        print('Recognition fail rate:', recognition_fail_rate)


if __name__ == '__main__':
    facerec = face_recognition_manager(test_data=False)
    # print(type(facerec))
    # facerec.test(0.55)
    facerec.realtime_recognition(CHATBOT_INTEGRATION=False,DEBUG=True,camera_index=0,tolerance=0.45,FLIP=False,ADDITIONAL_FACEINFO=True)

    #facerec.log_interaction(["Rabin Giri"])
    close_connection(facerec.base_db.con)
    close_connection(facerec.interaction_db.con)

