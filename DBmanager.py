import io
import os
import numpy as np
import sqlite3
from datetime import datetime
from frutils import create_directory


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        return conn
    except Exception as e:
        print(e)

    return None

def close_connection(connection):
    """
    Close connection of SQLite database
    :param connection:
    :return:
    """
    assert isinstance(connection,sqlite3.Connection)
    connection.close()


class FaceDatabaseManager:
    """
    Manages database for face recognition
    """
    
    def __init__(self,db_name = './Database/Face.db'):
        self.db_name = db_name
        create_directory(self.db_name)
        self.con = create_connection(self.db_name)

        # Create database
        cur = self.con.cursor()
        # Create table in database
        cur.execute("create table if not exists Faces ( name text, face_encoding array, filename text unique)")

    def adapt_array(self,arr):
        """
        Convert numpy array to text
        Note: 
        """
        
        out = io.BytesIO()
        np.save(out,arr)
        out.seek(0)
        return sqlite3.Binary(out.read())
        
        
    def convert_array(self,text):
        """
        Convert text to np.array
        """    
        out = io.BytesIO(text)
        out.seek(0)
        return np.load(out)

    def save_face_info(self,face_data):
        """
        Takes list of list consisting name,face_encoding,filename
        Create database called Face.db
        Create table called Faces containing three columns: filename, name,face
        Save  names ,face_encoding and corresponding filenames to database one at a time

        """

        # Converts np.array to TEXT when inserting
        sqlite3.register_adapter(np.ndarray, self.adapt_array)
        cur = self.con.cursor()

        count = 0
        for name,encoding,file in face_data:
            try:
                cur.execute("Insert into Faces VALUES (?,?,?)", (name,encoding,file))
                self.con.commit()
                count += 1
                print("Saving ",name)

            except Exception as e:
                print(e)

        print('Saving Face information into database completed')
        print('{} image information saved'.format(count))
        

    def read_faceinfo(self):
        """
        Read data from database
        :return:
        """
        
        # Converts TEXT to np.array when selecting
        sqlite3.register_converter("array", self.convert_array)

        try:
            cur = self.con.cursor()

            # Fetching Face encodings from Database
            cur.execute("select face_encoding from Faces")
            face_encodings = [encoding[0] for encoding in cur.fetchall()]

            # Fetching Labels from Database
            cur.execute("select name from Faces")
            names = [name[0] for name in cur.fetchall()]

            # Fetching Labels from Database
            cur.execute("select filename from Faces")
            filenames = [filename[0] for filename in cur.fetchall()]

            return face_encodings, names, filenames

        except Exception as e:
            print(e)


class InteractionDatabaseManager():

    def __init__(self,interaction_db='./Database/Interaction.db'):
        self.interaction_db = interaction_db
        create_directory(self.interaction_db)
        self.con = create_connection(self.interaction_db)
        # Create table in database
        cur = self.con.cursor()
        cur.execute("""create table if not exists Customers
                             (customer_id INTEGER PRIMARY KEY AUTOINCREMENT,name text unique)""")

        cur.execute("""Create table if not exists InteractionTime
                        (customer_id int,date_time timestamp,FOREIGN KEY(customer_id)
                         REFERENCES Customers(customer_id))""")

    def check_db(self,customer_names):
        """

        :param customer_names:
        :return:
        """
        date_time = datetime.now()

        cur = self.con.cursor()
        interactions = []

        for customer_name in customer_names:
            FIRST_INTERACTION = True
            interaction_times = []
            try:
                cur.execute("Insert into Customers (name) VALUES (?)", (customer_name,))
                self.con.commit()
            except sqlite3.IntegrityError:
                FIRST_INTERACTION = False
                print("Previous customer {}".format(customer_name))

            cur.execute("SELECT customer_id FROM Customers WHERE name=?", (customer_name,))
            customer_id = cur.fetchone()[0]

            if not FIRST_INTERACTION:
                cur.execute("SELECT date_time FROM InteractionTime WHERE customer_id=?", (customer_id,))
                interaction_times = cur.fetchall()

            cur.execute("INSERT INTO InteractionTime (customer_id,date_time) VALUES (?,?)",(customer_id,date_time))
            self.con.commit()

            interactions.append((customer_name,interaction_times))

        return interactions


if __name__ == "__main__":
    a = InteractionDatabaseManager()
    name = ["Bishal","Shyam","Dip","Niraj"]
    interactions = a.check_db(name)

    print(interactions)

