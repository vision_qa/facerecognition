# 		Face Recognition

#### Overview

Face recognition based on ResNet-34 which was trained in VGG dataset of 3M faces.This project contains two files : DBmanager.py and FRmanager.py. DBmanager.py handles database for face recognition.FRmanager.py provides different methods to carry out face recognition.

####Requirements

1. Python 3.5 or above
2. Dlib
3. face_recognition
4. opencv
5. Scipy
6. sklearn
7. numpy
8. sqlite3


#### Instructions

1. To create a base database ( database containing face encodings for comparison)  and test database, follow following steps:

   - Create directory containing face data

   ```
   main_directory
   |----person1_directory
   |		|----person1_image1.jpg
   |		|----person1_image2.jpg
   |		|----------------------
   |-----person2_directory
   |		|----person2_image1.jpg
   |		|----person2_image2.jpg
   |		|-----------
   |-----------
   |------------
   ```

   ​

   **Note:** At least 3 images per person gives best result. Image file name should be unique. If same image file name found ,(even in different directory) , its face encoding is not stored in database. (It is handled in DBmanager.py)


   - Create face_recognition_manager object

     ```python
     #When you already have your data
     import FRmanager as fr
     face_rec = fr.face_recognition_manager()
     ```

   - Creating train and test database

     ```python
     #Provide path for directory containing image directories
     image_path = '...' 
     
     #Create face_recognition_manager object and provide image path
     face_rec = fr.face_recognition_manager(data_path=image_path,test=False)
     
     #Note: test=True if the database you want to create is test database
     ```

​	This will create an database called **Face.db** in **Database** directory if ***test=False*** and similarly creates **test.db**  if  ***test =True*** 

2. Test model

   - Test can be simply carried out as

     ```python
     face_rec.test(0.55)
     ```

     Default parameters for test is **0.6 tolerance** 

     It uses **euclidean  distance** for comparison . One can use ***cosine = True*** to use **cosine similarity**.

     Test result is stored in **TestLog.txt** file.

     **Note:** Euclidean distance comparison give best result when tolerance is 0.55

   ​

   ​