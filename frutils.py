import time
import os
import glob
from shutil import copy2
from datetime import datetime


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result

    return timed


def time_dif(t1, t2):
    """
    Compute difference in time given in format '12:30:16'.
    Return value in minutes
    :param t1:
    :param t2:
    :return:
    """
    h1, m1, s1 = t1.hour, t1.minute, t1.second
    h2, m2, s2 = t2.hour, t2.minute, t2.second
    t1_mins = s1 / 60 + (m1 + 60 * h1)
    t2_mins = s2 / 60 + (m2 + 60 * h2)

    return t1_mins - t2_mins


def date_dif(date1, date2):
    """

    :param date1:
    :param date2:
    :return: time difference in minute
    """
    year1, month1, day1 = date1.year, date1.month, date1.day
    year2, month2, day2 = date2.year, date2.month, date2.day

    day_dif = (year1 - year2) * 365 + (month1 - month2) * 30 + (day1 - day2)

    # 1 day = 1440 minutes
    return day_dif * 1440


def train_test_split(path, split):
    """
    Split data into train and test data.

    :param path: path for image data
    :param split: percent of train data ( beetween 1 and 0)
    :return:
    """

    train_dir = './Train'
    test_dir = './Test'

    if not os.path.exists(train_dir):
        os.makedirs(train_dir)
    if not os.path.exists(test_dir):
        os.makedirs(test_dir)

    for dirname, dirnames, filenames in os.walk(path):
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            files = glob.glob(os.path.join(subject_path, '*.jpg'))
            total_files = len(files)
            for filenum, filename in enumerate(files):
                if filenum <= int(split * total_files):
                    save_dir = os.path.join(train_dir, subdirname)
                else:
                    save_dir = os.path.join(test_dir, subdirname)

                if not os.path.exists(save_dir):
                    os.makedirs(save_dir)

                copy2(filename, save_dir)


def check_interactions(interactions):
    """

    :param interactions:
    :return:
    """

    # Variable to hold customer names and their interaction information
    interaction_record = []
    for interaction in interactions:
        NEW_CLIENT = False
        RECENTLY_INTERACTED = False

        name = interaction[0]

        # Check whether the customer in a new client or previously interacted client
        if len(interaction[1]) > 0:
            interaction_datetime = interaction[1]
            latest_interaction = interaction_datetime[-1][0]

            interaction_time = latest_interaction.time()
            current_time = datetime.now().time()
            interaction_date = latest_interaction.date()
            current_date = datetime.now().date()

            # Calculate time difference between current time and last interaction time
            interaction_timedif = date_dif(current_date, interaction_date) + time_dif(current_time, interaction_time)

            if interaction_timedif < 60:
                RECENTLY_INTERACTED = True
        else:
            NEW_CLIENT = True

        interaction_record.append([name, NEW_CLIENT, RECENTLY_INTERACTED])

        print(interaction_record)
        return interaction_record


def makeFilenamesUnique(dir):
    """
    Make filenames unique
    Dir/Dir1/1 --> dir/dir1/dir1_1
    Dir/Dir2/1 --> dir/dir2/dir2_1
    """

    user_dirs = os.listdir(dir)  # List of persons in the dir

    for person in user_dirs:
        person = os.path.split(person)[-1]  # get only the last folder name in the directory name
        image_names = os.listdir(os.path.join(dir, person))  # get all the files in the person dir
        # filter out non image files
        image_names = [name for name in image_names if name.endswith(".jpg") or name.endswith(".png")]
        new_image_names = ["{}__{}".format(person, image_name) for image_name in image_names]

        person_dir = os.path.join(dir, person)
        for oldname, newname in zip(image_names, new_image_names):
            newname = os.path.join(person_dir, newname)
            oldname = os.path.join(person_dir, oldname)

            if newname.count('__') == 1:
                os.rename(oldname, newname)
            #print(oldname, newname)

def create_directory(filepath):
    """

    :param filepath:
    :return:
    """
    directory_path = os.path.dirname(os.path.abspath(filepath))
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)


if __name__ == '__main__':
    path = './Images'
    # train_test_split(path,0.1)
    # print( time.time())

