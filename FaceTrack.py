import dlib
import cv2
import threading
import time
from FRmanager import face_recognition_manager as frm
#from face_recognition import face_locations


face_rec = frm()
face_database = face_rec.base_db
face_in_db,label_in_db,_ = face_database.read_faceinfo()

def rect_to_boundingbox(rects):
    # take a bounding predicted by dlib and convert it
    # to the format (x, y, w, h)
    rect_cordinates = []
    for rect in rects:
        x = int(rect.left())
        y = int(rect.top())
        h = int(rect.height())
        w = int(rect.width())
        rect_cordinates.append((x, y, w, h))

    return rect_cordinates

def _rect_to_css(rect):
    """
    Convert a dlib 'rect' object to a plain tuple in (top, right, bottom, left) order
    """
    return rect.top(), rect.right(), rect.bottom(), rect.left()

def _trim_css_to_bounds(css, image_shape):
    """
    Make sure a tuple in (top, right, bottom, left) order is within the bounds of the image.
    """
    return max(css[0], 0), min(css[1], image_shape[1]), min(css[2],
                image_shape[0]), max(css[3], 0)

# Setting faceid
def doRecognizePerson(face_location,image,MULTRACK,faceNames=None,fid=None):
    "Find name of detected person"
    time.sleep(0.1)
    face, label = face_in_db,label_in_db
    name = face_rec.recognize_face_in_image(face,label,image,face_location,tolerance=0.5)
    if MULTRACK:
        if name:
            faceNames[fid] = name
        else:
            faceNames[fid] = "Person " + str(fid)
    else:
        return name if name else 'Unknown'


def detect_and_track_face(MULTRACK=False,FACEREC=True,VIDREC=False,FLIP=True,CAMERA=0,DEBUG=False):
    " Detect and track either largest or multiple faces  "
    capture = cv2.VideoCapture(CAMERA)
    # Create Face detector
    detector = dlib.get_frontal_face_detector()
    # Create the tracker
    tracker = dlib.correlation_tracker()
    # Flag for tracking largest face
    trackingFace = 0

    frameCounter = 0
    currentFaceID = 0

    # Variables holding the correlation trackers and the name per faceid
    faceTrackers = {}
    faceNames = {}
    faceCentroids = []

    rectangleColor = (0, 165, 255)
    font = cv2.FONT_HERSHEY_SIMPLEX
    # The deisred output width and height
    OUTPUT_SIZE_WIDTH = 640
    OUTPUT_SIZE_HEIGHT = 480
    resize_ratio = 2
    first_resize = True

    if VIDREC:
        # Define the codec and create VideoWriter object
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter('output_track_wasim3.avi', fourcc, 8.0, (640, 480))

    try:
        while True:
            rc, frame = capture.read()

            if FLIP:
                frame = cv2.flip(frame,-1)

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            if first_resize:
                (h, w, i) = frame.shape
                resizeX, resizeY = (w // resize_ratio, h // resize_ratio)
                image_centroid = (resizeX // 2, resizeY // 2)
                first_resize = False

            baseImage = cv2.resize(gray, (resizeX, resizeY))
            baseImageColor = cv2.resize(frame,(resizeX,resizeY))

            if DEBUG:
                # To output image in color
                frame = cv2.resize(frame, (resizeX, resizeY))
                resultImage = frame.copy()
            else:
                resultImage = baseImage.copy()

            if MULTRACK:

                # STEPS
                # * Update all trackers and remove the ones that are not 
                #   relevant anymore
                # * Every 10 frames:
                #       + Use face detection on the current frame and look
                #         for faces. 
                #       + For each found face, check if centerpoint is within
                #         existing tracked box. If so, nothing to do
                #       + If centerpoint is NOT in existing tracked box, then
                #         we add a new tracker with a new face-id


                # Increase the framecounter
                frameCounter += 1

                # Update all the trackers and remove the ones for which the update
                # indicated the quality was not good enough
                fidsToDelete = []
                for fid in faceTrackers.keys():
                    trackingQuality = faceTrackers[fid].update(baseImage)

                    # If the tracking quality is good enough,delete tracker
                    if trackingQuality < 7:
                        fidsToDelete.append(fid)

                for fid in fidsToDelete:
                    print("Removing fid " + str(fid) + " from list of trackers")
                    faceTrackers.pop(fid, None)

                # Every 10 frames,determine which are present in the frame
                if (frameCounter % 10) == 0:
                    # For the face detection, use a gray image
                    # gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
                    faces = detector(baseImage)
                    # Convert dlib rectangle to (x,y,w,h)
                    face_rects = rect_to_boundingbox(faces)

                    #Convert detected face in format required for face recognition
                    face_locations = [_trim_css_to_bounds(_rect_to_css(face), baseImage.shape)
                                     for face in faces]

                    assert len(face_rects) == len(face_locations)

                    for i,(x, y, w, h) in enumerate(face_rects):
                        # calculate the centerpoint
                        x_bar = x + 0.5 * w
                        y_bar = y + 0.5 * h

                        # Variable holding information which faceid that is matched
                        matchedFid = None

                        # Now loop over all the trackers and check if the
                        # centerpoint of the face is within the box of a
                        # tracker
                        for fid in faceTrackers.keys():
                            tracked_position = faceTrackers[fid].get_position()

                            (t_x, t_y, t_w, t_h) = rect_to_boundingbox([tracked_position])[0]

                            # calculate the centerpoint
                            t_x_bar = t_x + 0.5 * t_w
                            t_y_bar = t_y + 0.5 * t_h

                            # check if the centerpoint of the face is within the
                            # rectangleof a tracker region. Also, the centerpoint
                            # of the tracker region must be within the region
                            # detected as a face. If both of these conditions a match occur
                            if ((t_x <= x_bar <= (t_x + t_w)) and
                                    (t_y <= y_bar <= (t_y + t_h)) and
                                    (x <= t_x_bar <= (x + w)) and
                                    (y <= t_y_bar <= (y + h))):
                                matchedFid = fid

                        # If no matched fid, then we have to create a new tracker
                        if matchedFid is None:
                            print("Creating new tracker " + str(currentFaceID))

                            # Create and store the tracker
                            tracker = dlib.correlation_tracker()

                            #Convert tracking rectangle to dlib rectangle
                            dlib_rects = dlib.rectangle(x - 10, y - 20, x + w + 10, y + h + 20)

                            tracker.start_track(baseImage, dlib_rects)

                            faceTrackers[currentFaceID] = tracker
                            face_location = [face_locations[i]]
                            # Start a new thread to track faceid
                            if FACEREC:
                                t = threading.Thread(target=doRecognizePerson,
                                                     args=(face_location,baseImageColor,
                                                           MULTRACK,faceNames, currentFaceID,))
                                t.start()
                            # Increase the currentFaceID counter
                            currentFaceID += 1

                # loop over all the trackers and draw the rectangle
                # around the detected faces.
                for fid in faceTrackers.keys():
                    tracked_position = faceTrackers[fid].get_position()

                    (t_x, t_y, t_w, t_h) = rect_to_boundingbox([tracked_position])[0]

                    # Centroid of face
                    face_centX = int(t_x + 0.5 * t_x)
                    face_centY = int(t_y + 0.5 * t_h)
                    faceCentroids.append((face_centX, face_centY))

                    if DEBUG:
                        cv2.rectangle(resultImage, (t_x, t_y), (t_x + t_w, t_y + t_h),
                                      rectangleColor, 1)

                        if FACEREC:
                            if fid in faceNames.keys():
                                cv2.putText(resultImage, faceNames[fid],
                                            (int(t_x + t_w / 2), t_y), font,
                                            0.3, (255, 255, 255), 1)
                            else:
                                cv2.putText(resultImage, "Detecting...",
                                            (int(t_x + t_w / 2), t_y), font,
                                            0.3, (255, 255, 255), 1)

            else:
                # If not tracking a face, then try to detect face
                if not trackingFace:
                    print('Not tracking')
                    # Dlib face detector
                    faces = detector(baseImage, 0)
                    face_rects = rect_to_boundingbox(faces)
                    face_locations = [_trim_css_to_bounds(_rect_to_css(face), baseImage.shape)
                                      for face in faces]

                    if len(face_rects) > 0:
                        # If one or more faces are found, initialize the tracker
                        # on the largest face in the picture

                        largest_face = max(face_rects, key=lambda x: (x[2] * x[3]))
                        x, y, w, h = largest_face
                        # Initialize the tracker
                        dlib_rects = dlib.rectangle(x - 10, y - 20, x + w + 10, y + h + 20)
                        if FACEREC:
                            name = doRecognizePerson(face_locations,baseImageColor,MULTRACK)
                        tracker.start_track(baseImage, dlib_rects)

                        # Set the indicator variable such that we know the
                        # tracker is tracking a region in the image
                        trackingFace = 1

                # Check if the tracker is actively tracking a region in the image
                if trackingFace:
                    # Update the tracker and request information about the
                    # quality of the tracking update
                    trackingQuality = tracker.update(baseImage)

                    # If the tracking quality is good enough, determine the
                    # updated position of the tracked region and draw rectangle
                    if trackingQuality >= 8.75:
                        tracked_position = tracker.get_position()
                        (t_x, t_y, t_w, t_h) = rect_to_boundingbox([tracked_position])[0]

                        # Centroid of face
                        face_centX = int(t_x + 0.5 * t_x)
                        face_centY = int(t_y + 0.5 * t_h)

                        if DEBUG:
                            cv2.rectangle(resultImage, (t_x, t_y), (t_x + t_w, t_y + t_h),
                                          rectangleColor, 1)
                            if FACEREC:
                                cv2.putText(resultImage,name,(int(t_x + t_w / 2), t_y), font,
                                            0.3, (255, 255, 255), 1)

                    else:
                        # If the quality of the tracking update is not
                        # sufficient (e.g. the tracked region moved out of the
                        # screen) we stop the tracking of the face
                        trackingFace = 0

            if DEBUG:
                largeResult = cv2.resize(resultImage,
                                         (OUTPUT_SIZE_WIDTH, OUTPUT_SIZE_HEIGHT))

                cv2.imshow("result-image", largeResult)

                if VIDREC:
                    out.write(largeResult)
                # press q to quit
                # Control FPS by changing waitFey value
                if cv2.waitKey(100) == ord('q'):
                    break

        capture.release()
        if VIDREC:
            out.release()

        cv2.destroyAllWindows()

    except Exception as e:
        raise
        print(e)
        exit(0)


if __name__ == '__main__':
    detect_and_track_face(MULTRACK=True,FACEREC=True,VIDREC=False,CAMERA=0,DEBUG=True,FLIP=False)
